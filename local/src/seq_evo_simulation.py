#!/usr/bin/env python
from __future__ import with_statement
from sys import stdin, stderr, stdout
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
import random
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import numpy as np
from scipy.linalg import expm
import pandas as pd
import itertools
import math

import re
import os
import distance

codons = ['AAA', 'AAC', 'AAG', 'AAT', 'ACA', 'ACC', 'ACG', 'ACT', 'AGA', 'AGC', 'AGG', 'AGT', 'ATA', 'ATC', 'ATG', 'ATT', 'CAA', 'CAC', 'CAG', 'CAT', 'CCA', 'CCC', 'CCG', 'CCT', 'CGA', 'CGC', 'CGG', 'CGT', 'CTA', 'CTC', 'CTG', 'CTT', 'GAA', 'GAC', 'GAG', 'GAT', 'GCA', 'GCC', 'GCG', 'GCT', 'GGA', 'GGC', 'GGG', 'GGT', 'GTA', 'GTC', 'GTG', 'GTT', 'TAC', 'TAT', 'TCA', 'TCC', 'TCG', 'TCT', 'TGC', 'TGG', 'TGT', 'TTA', 'TTC', 'TTG', 'TTT']
gen_code_dict = {'CTT': 'Leucine', 'ATG': 'Methionine', 'ACA': 'Threonine', 'ACG': 'Threonine', 'ATC': 'Isoleucine', 'AAC': 'Asparagine', 'ATA': 'Isoleucine', 'AGG': 'Arginine', 'CCT': 'Proline', 'ACT': 'Threonine', 'AGC': 'Serine', 'AAG': 'Lysine', 'AGA': 'Arginine', 'CAT': 'Histidine ', 'AAT': 'Asparagine', 'ATT': 'Isoleucine', 'CTG': 'Leucine', 'CTA': 'Leucine', 'CTC': 'Leucine', 'CAC': 'Histidine ', 'AAA': 'Lysine', 'CCG': 'Proline', 'AGT': 'Serine', 'CCA': 'Proline', 'CAA': 'Glutamine', 'CCC': 'Proline', 'TAT': 'Tyrosine', 'GGT': 'Glycine', 'TGT': 'Cysteine', 'CGA': 'Arginine', 'CAG': 'Glutamine', 'TCT': 'Serine', 'GAT': 'Aspartic acid', 'CGG': 'Arginine', 'TTT': 'Phenylalanine', 'TGC': 'Cysteine', 'GGG': 'Glycine', 'TAG': 'Stop codons', 'GGA': 'Glycine', 'TGG': 'Tryptophan', 'GGC': 'Glycine', 'TAC': 'Tyrosine', 'TTC': 'Phenylalanine', 'TCG': 'Serine', 'TTA': 'Leucine', 'TTG': 'Leucine', 'TCC': 'Serine', 'ACC': 'Threonine', 'TCA': 'Serine', 'GCA': 'Alanine', 'GTA': 'Valine', 'GCC': 'Alanine', 'GTC': 'Valine', 'GCG': 'Alanine', 'GTG': 'Valine', 'GAG': 'Glutamic acid', 'GTT': 'Valine', 'GCT': 'Alanine', 'TGA': 'Stop codons', 'GAC': 'Aspartic acid', 'CGT': 'Arginine', 'GAA': 'Glutamic acid', 'TAA': 'Stop codons', 'CGC': 'Arginine'}
def main():
	usage = format_usage('''
		%prog FREQS < ORIG_MATRIX
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-T', '--Timepoints', default=False, help='custom timepoints, symbols must be comma-separated [default: %default]')
	parser.add_option('-r', '--runs', type="int", default=False, help='number of simulation runs to be performed [default: 5]')
	parser.add_option('-l', '--length', type="int", default=1000, help='length of sequence to be generated [default: 100]')
	parser.add_option('-i', '--identity', default=False, action="store_true", help='calculate the % AA sequence identity and % codon sequence identity [default: %default]')	
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')

	nstates = len(codons)
	orig_matrix = np.zeros((nstates,nstates), dtype='float64')
	
	
	# parse original matrix
	i = 0
	for line in stdin:
		orig_matrix[i,:] = safe_rstrip(line).split('\t')
		i = i + 1
	
	# parse freqs
	freqs = pd.read_csv(args[0], sep="\t").values.flatten()
	
	# specify sequence length 
	seq_len = options.length
	
	# from IRM to Q with lines that sum to 0
	for i in range(0,nstates):
		orig_matrix[i,i] = (np.sum(orig_matrix[i,:]) - orig_matrix[i,i])*-1
	
	# specify timeponts
	if options.Timepoints: 
		timepoints = [float(x) for x in options.Timepoints.split(",")]
	else:
		timepoints = np.arange(0, 10, 0.1)
	
	# specify the number of simulation runs
	# i.e. the number of sequences generated at every timepoint
	if options.runs:
		runs = options.runs
	else:
		runs = 1

	def seq_gen(freqs, seq_len, states):
		seq = np.random.choice(states, seq_len, p=freqs)
		return seq
	
	phy_header = "2\t%d" % (seq_len)
	for run_num in range(runs):
		for t in timepoints:	
			# generate an ancestor sequence at every timepoint to avoid any composition bias
			orig_seq = seq_gen(freqs, seq_len, codons)
			new_seq = np.zeros(seq_len, dtype=object)
			# calculate P(t)
			Pt_matrix = expm(t* orig_matrix)
			nucl_diffs = 0
			codon_diffs = 0
			AA_diffs = 0
			for site,current_state in enumerate(orig_seq):
				P_state = Pt_matrix[codons.index(current_state),:]
				# simulate a change of state according to P(t)
				new_state = np.random.choice(codons, 1, p=P_state)[0]
				new_seq[site] = new_state
				if new_state != current_state:
					codon_diffs += 1
					for current_nucl, new_nucl in zip(list(current_state), list(new_state)):
						if new_nucl != current_nucl:
							nucl_diffs += 1
					if gen_code_dict[new_state] != gen_code_dict[current_state]:
						AA_diffs += 1
			nucl_identity = (seq_len*3 - float(nucl_diffs)) / (seq_len*3)
			codon_identity = (seq_len - float(codon_diffs)) / seq_len
			AA_identity = (seq_len - float(AA_diffs)) / seq_len
			# output a PHYLIP-style alignment of each simulated sequence against the original sequence
			if options.identity:
				print  "NA\tNA\tNA\t" + phy_header
				print  "%f\t%f\t%f\t%f\tOrig_seq-%d\t%s" % (nucl_identity,codon_identity,AA_identity,t,run_num, ' '.join(map(str, orig_seq)))
				print  "%f\t%f\t%f\t%f\tNew_seq-%d\t%s" % (nucl_identity,codon_identity,AA_identity,t, run_num,' '.join(map(str, new_seq)))
				print   "NA\tNA\tNA"
			else:
				print  phy_header
				print  "Orig_seq-%f-%d\t%s" % (t, run_num, ' '.join(map(str, orig_seq)))
				print  "New_seq-%f-%d\t%s" % (t, run_num,' '.join(map(str, new_seq)))
				print

if __name__ == '__main__':
	main()
