#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import numpy as np
import pandas as pd
from functools import reduce
from operator import mul

codons = ['AAA', 'AAC', 'AAG', 'AAT', 'ACA', 'ACC', 'ACG', 'ACT', 'AGA', 'AGC', 'AGG', 'AGT', 'ATA', 'ATC', 'ATG', 'ATT', 'CAA', 'CAC', 'CAG', 'CAT', 'CCA', 'CCC', 'CCG', 'CCT', 'CGA', 'CGC', 'CGG', 'CGT', 'CTA', 'CTC', 'CTG', 'CTT', 'GAA', 'GAC', 'GAG', 'GAT', 'GCA', 'GCC', 'GCG', 'GCT', 'GGA', 'GGC', 'GGG', 'GGT', 'GTA', 'GTC', 'GTG', 'GTT', 'TAC', 'TAT', 'TCA', 'TCC', 'TCG', 'TCT', 'TGC', 'TGG', 'TGT', 'TTA', 'TTC', 'TTG', 'TTT']
def main():
	usage = format_usage('''
		%prog < NUCL_FREQ_TAB
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=0.05, help='some help CUTOFF [default: %default]', metavar='CUTOFF')
	parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE [default: %default]', metavar='FILE')
	parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help [default: %default]')
	options, args = parser.parse_args()

	data = pd.read_csv(stdin, sep="\t").set_index('Position')

	if len(args) != 0:
		exit('Unexpected argument number.')

	# calculate each codon frequency using PiJ = pij1 * pij2 * pij3
	codon_freqs = []
	for codon in codons:
		nucs = list(codon)
		codon_freqs.append(reduce(mul, [data.loc[str(idx + 1),"pi" + nuc] for idx,nuc in enumerate(nucs)], 1))
	# calculate the scaling factor C
	C = 1 / sum(codon_freqs)
	scaled_codon_freqs = [f*C for f in codon_freqs]
	
	print '\t'.join(map(str, codons))
	print '\t'.join(map(str, scaled_codon_freqs))


if __name__ == '__main__':
	main()

