#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import numpy as np
import pandas as pd 

gen_code_dict = {'CTT': 'Leucine', 'ATG': 'Methionine', 'ACA': 'Threonine', 'ACG': 'Threonine', 'ATC': 'Isoleucine', 'AAC': 'Asparagine', 'ATA': 'Isoleucine', 'AGG': 'Arginine', 'CCT': 'Proline', 'ACT': 'Threonine', 'AGC': 'Serine', 'AAG': 'Lysine', 'AGA': 'Arginine', 'CAT': 'Histidine ', 'AAT': 'Asparagine', 'ATT': 'Isoleucine', 'CTG': 'Leucine', 'CTA': 'Leucine', 'CTC': 'Leucine', 'CAC': 'Histidine ', 'AAA': 'Lysine', 'CCG': 'Proline', 'AGT': 'Serine', 'CCA': 'Proline', 'CAA': 'Glutamine', 'CCC': 'Proline', 'TAT': 'Tyrosine', 'GGT': 'Glycine', 'TGT': 'Cysteine', 'CGA': 'Arginine', 'CAG': 'Glutamine', 'TCT': 'Serine', 'GAT': 'Aspartic acid', 'CGG': 'Arginine', 'TTT': 'Phenylalanine', 'TGC': 'Cysteine', 'GGG': 'Glycine', 'TAG': 'Stop codons', 'GGA': 'Glycine', 'TGG': 'Tryptophan', 'GGC': 'Glycine', 'TAC': 'Tyrosine', 'TTC': 'Phenylalanine', 'TCG': 'Serine', 'TTA': 'Leucine', 'TTG': 'Leucine', 'TCC': 'Serine', 'ACC': 'Threonine', 'TCA': 'Serine', 'GCA': 'Alanine', 'GTA': 'Valine', 'GCC': 'Alanine', 'GTC': 'Valine', 'GCG': 'Alanine', 'GTG': 'Valine', 'GAG': 'Glutamic acid', 'GTT': 'Valine', 'GCT': 'Alanine', 'TGA': 'Stop codons', 'GAC': 'Aspartic acid', 'CGT': 'Arginine', 'GAA': 'Glutamic acid', 'TAA': 'Stop codons', 'CGC': 'Arginine'} 
trans_transv_dict = {'AA': 'Transition', 'AC': 'Transversion', 'GT': 'Transversion', 'AG': 'Transition', 'CC': 'Transition', 'CA': 'Transversion', 'CG': 'Transversion', 'TT': 'Transition', 'GG': 'Transition', 'GC': 'Transversion', 'AT': 'Transversion', 'GA': 'Transition', 'TG': 'Transversion', 'TA': 'Transversion', 'TC': 'Transition', 'CT': 'Transition'}

def main():
	usage = format_usage('''
		%prog OMEGA KAPPA
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-f', '--freq_file', default=False, help='Read frequencies from file [default: %default]', metavar='FILE')

	options, args = parser.parse_args()
	omega = float(args[0]) 
	if options.freq_file:
			freqs = pd.read_csv(options.freq_file, sep="\t").values.flatten()
	else:
		freqs = np.array([1./61]*61).flatten()
	kappa = float(args[1])
	IRM = np.zeros((61,61), dtype='float64')

	if len(args) != 2:
		exit('Unexpected argument number.')
	
	codons = sorted([x for x in gen_code_dict.keys() if gen_code_dict[x] != "Stop codons"])
	for i, col_codon in enumerate(codons):
		A = list(col_codon)
		for j, row_codon in enumerate(codons):
			# find out if synonimous or non-synonimous
			synonimous = False
			if gen_code_dict[col_codon] == gen_code_dict[row_codon]:
				synonimous = True

			B = list(row_codon)
			# count the differences (i.e. nucleotide substitutions)
			diffs = []
			for idx,nucl in enumerate(A):
				if nucl != B[idx]:
					# look up if it is a transition or a transversion:
					diffs.append(trans_transv_dict[nucl + B[idx]])
		
			# calculate the IR for codon i and j according to Goldman and Yang (1994),
			# i.e the simplified version of the model detailed in Yang, Ziheng. Molecular Evolution: A Statistical Approach (p. 43). OUP Oxford. Kindle Edition.
			if len(diffs) == 1:
				if synonimous == True and diffs[0] == 'Transversion':
					IRM[i][j] = freqs[j]
				elif synonimous == True and diffs[0] == 'Transition':
					IRM[i][j] = freqs[j]*kappa
				elif synonimous == False and diffs[0] == 'Transversion':
					IRM[i][j] = freqs[j]*omega
				elif synonimous == False and diffs[0] == 'Transition':
					IRM[i][j] = freqs[j]*omega*kappa
	header = ['X'] + codons
	print  '\t'.join(map(str, header))
	for idx, symbol in enumerate(codons):
		row = [symbol]
		row.extend([x for x in IRM[idx,:]])
		print '\t'.join(map(str, row))
	
if __name__ == '__main__':
	main()

