#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader
import numpy as np

codons = ['AAA', 'AAC', 'AAG', 'AAT', 'ACA', 'ACC', 'ACG', 'ACT', 'AGA', 'AGC', 'AGG', 'AGT', 'ATA', 'ATC', 'ATG', 'ATT', 'CAA', 'CAC', 'CAG', 'CAT', 'CCA', 'CCC', 'CCG', 'CCT', 'CGA', 'CGC', 'CGG', 'CGT', 'CTA', 'CTC', 'CTG', 'CTT', 'GAA', 'GAC', 'GAG', 'GAT', 'GCA', 'GCC', 'GCG', 'GCT', 'GGA', 'GGC', 'GGG', 'GGT', 'GTA', 'GTC', 'GTG', 'GTT', 'TAC', 'TAT', 'TCA', 'TCC', 'TCG', 'TCT', 'TGC', 'TGG', 'TGT', 'TTA', 'TTC', 'TTG', 'TTT']

def main():
	usage = format_usage('''
		%prog SEQ_LEN <FREQS
	''')
	parser = OptionParser(usage=usage)
	parser.add_option('-o', '--one_to_three', dest='one_to_three', action='store_true', default=False, help='1-letter AA to 3-letter AA [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')

	seq_len = int(args[0])

	for line in stdin:
		freqs = [float(x) for x in safe_rstrip(line).split('\t')]
	
	seq = np.random.choice(codons, seq_len, p=freqs)

	print "%s" % (' '.join(map(str, seq)))	

if __name__ == '__main__':
	main()

