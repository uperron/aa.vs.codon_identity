#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader
import os
import re

gen_code_dict = {'CTT': 'Leucine', 'ATG': 'Methionine', 'ACA': 'Threonine', 'ACG': 'Threonine', 'ATC': 'Isoleucine', 'AAC': 'Asparagine', 'ATA': 'Isoleucine', 'AGG': 'Arginine', 'CCT': 'Proline', 'ACT': 'Threonine', 'AGC': 'Serine', 'AAG': 'Lysine', 'AGA': 'Arginine', 'CAT': 'Histidine ', 'AAT': 'Asparagine', 'ATT': 'Isoleucine', 'CTG': 'Leucine', 'CTA': 'Leucine', 'CTC': 'Leucine', 'CAC': 'Histidine ', 'AAA': 'Lysine', 'CCG': 'Proline', 'AGT': 'Serine', 'CCA': 'Proline', 'CAA': 'Glutamine', 'CCC': 'Proline', 'TAT': 'Tyrosine', 'GGT': 'Glycine', 'TGT': 'Cysteine', 'CGA': 'Arginine', 'CAG': 'Glutamine', 'TCT': 'Serine', 'GAT': 'Aspartic acid', 'CGG': 'Arginine', 'TTT': 'Phenylalanine', 'TGC': 'Cysteine', 'GGG': 'Glycine', 'TAG': 'Stop codons', 'GGA': 'Glycine', 'TGG': 'Tryptophan', 'GGC': 'Glycine', 'TAC': 'Tyrosine', 'TTC': 'Phenylalanine', 'TCG': 'Serine', 'TTA': 'Leucine', 'TTG': 'Leucine', 'TCC': 'Serine', 'ACC': 'Threonine', 'TCA': 'Serine', 'GCA': 'Alanine', 'GTA': 'Valine', 'GCC': 'Alanine', 'GTC': 'Valine', 'GCG': 'Alanine', 'GTG': 'Valine', 'GAG': 'Glutamic acid', 'GTT': 'Valine', 'GCT': 'Alanine', 'TGA': 'Stop codons', 'GAC': 'Aspartic acid', 'CGT': 'Arginine', 'GAA': 'Glutamic acid', 'TAA': 'Stop codons', 'CGC': 'Arginine'}


def main():
	usage = format_usage('''
		%prog PML_OUT_DIR
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-c', '--cutoff', type=float, dest='cutoff', default=0.05, help='some help CUTOFF [default: %default]', metavar='CUTOFF')
	parser.add_option('-p', '--dump-params', dest='params_file', help='some help FILE [default: %default]', metavar='FILE')
	parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='some help [default: %default]')

	options, args = parser.parse_args()
	
	if len(args) != 1:
		exit('Unexpected argument number.')
	
	run = 0
	for filename in os.listdir(args[0]):
		#omega_0.2_treelen_0.1.paml
		path = args[0] + "/" + filename
		omega, t = re.findall("\d+\.\d+", filename)
		with open(path, 'r') as fd:
			seqs = []
			for line in fd:
				if line != ['\n'] and len(line) > 100:
					seqs.append(line.split()[1:])
		orig_seq, new_seq = seqs
		codon_diffs = 0
		nucl_diffs = 0
		AA_diffs = 0
		seq_len = len(orig_seq)
		for site,current_state in enumerate(orig_seq):
			new_state = new_seq[site]
			if new_state != current_state:
				codon_diffs += 1
				for current_nucl, new_nucl in zip(list(current_state), list(new_state)):
					if new_nucl != current_nucl:
						nucl_diffs += 1
				if gen_code_dict[new_state] != gen_code_dict[current_state]:
					AA_diffs += 1
		nucl_identity = (seq_len*3 - float(nucl_diffs)) / (seq_len*3)
		codon_identity = (seq_len - float(codon_diffs)) / seq_len
		AA_identity = (seq_len - float(AA_diffs)) / seq_len
		print '\t'.join(map(str, [omega, run, t, nucl_identity, codon_identity, AA_identity]))

if __name__ == '__main__':
	main()

