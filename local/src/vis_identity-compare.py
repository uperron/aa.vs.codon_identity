#!/usr/bin/env python
from __future__ import with_statement

from sys import stdin, stderr
from optparse import OptionParser
from vfork.io.util import safe_rstrip
from vfork.util import exit, format_usage
#from random import shuffle
#from subprocess import Popen, PIPE
#from collections import defaultdict
#from vfork.io.colreader import Reader

import matplotlib
# force matplotlib  not to use Xwi
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker 
import matplotlib.patches as mpatches
import pandas as pd
import numpy as np
import itertools

def main():
	usage = format_usage('''
		%prog <STDIN
	STDIN:
	1	omega
	2	run
	3	t
	4	nucl_identity
	5	codon_identity
	6	AA_identity
	''')
	parser = OptionParser(usage=usage)
	
	parser.add_option('-o', '--outfile', dest='outfile', default='outfile', help='output file [default: %default]')
	options, args = parser.parse_args()
	
	if len(args) != 0:
		exit('Unexpected argument number.')
	

	colors = ['#0000ff', '#aecc5a', '#fda02e', '#ab1010', '#7a2762', '#4ad4ff', '#ffc0cb', '#631919']
	fig = plt.figure(figsize=(12,12))
	ax = fig.add_subplot(111)
	#plt.gca().invert_xaxis()
	#plt.gca().invert_yaxis()

	# parse stdin and plot the boxplots
	data = pd.read_csv(stdin, sep="\t")
	# group by omega
	grouped = data.groupby('omega', sort=False)
	labels = sorted(data['omega'].drop_duplicates().values)

	for label, color in zip(labels, colors):
		x = grouped.get_group(label)['AA_identity'].values
		y = grouped.get_group(label)['nucl_identity'].values
		t_labels = grouped.get_group(label)['t'].values
		plt.plot(x, y, label="omega=" + str(label), color=color, fillstyle='full', marker='.', markersize=4, markerfacecolor='black', markeredgecolor='black')
	# annotate  markers with the corresponding t value if integer
	int_t_labels = t_labels[1::10]
	int_x = x[1::10]
	int_y = y[1::10]
	for t_label, X, Y in zip(int_t_labels, int_x, int_y):
		plt.annotate(
				t_label,
				xy=(X, Y), xytext=(-20, 20),
				textcoords='offset points', ha='right', va='bottom',
				arrowprops=dict(arrowstyle = '-', connectionstyle='arc3,rad=0'))

	plt.xlabel('AA_identity')
	plt.ylabel('DNA_identity')
	plt.legend()

	plt.savefig(options.outfile, format='pdf', bbox_inches='tight', dpi=fig.dpi)

if __name__ == '__main__':
	main()

