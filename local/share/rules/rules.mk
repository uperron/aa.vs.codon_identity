# Kappa value is taken from Yang's rbcL genes example (Yang, Ziheng. Molecular Evolution: A Statistical Approach (p. 50). OUP Oxford. Kindle Edition.)
%omega_$(KAPPA)kappa_Fequal_IRM: 
	../../local/src/IRM_from_params.py $* $(KAPPA) >$@

%omega_$(KAPPA)kappa_F3x4_IRM: Yang_rbcL-genes_F3x4.freqs 
	../../local/src/IRM_from_params.py -f $< $* $(KAPPA) >$@

fequal.freqs:
	printf "$(CODONS)\n" > $@ ; \
	printf "$(FEQUAL)" | tr ' ' '\t' l.orig-seq>> $@

Yang_rbcL-genes_F3x4.freqs: ../../local/share/data/Yang_rbcL-genes_example.tsv
	cat $< | ../../local/src/F3x4.py >$@

PHONY: MAKE_FEQUAL_IRMS MAKE_F3x4_IRMS MAKE_F3x4_SEQUENCES.PRE

%omega_$(KAPPA)kappa_F3x4_$(SEQ_LEN)sequences.pre: Yang_rbcL-genes_F3x4.freqs %omega_$(KAPPA)kappa_F3x4_IRM
	cut -f2- $^2 | unhead | ../../local/src/seq_evo_simulation.py -l $(SEQ_LEN) -i $< >$@

.META: *sequences.pre
	1	nucl_identity
	2	codon_identity
	3	AA_identity
	4	t
	5	seq_name
	6	seq

ALLomega_$(KAPPA)kappa_F3x4_$(SEQ_LEN)sequences.tab: *omega_$(KAPPA)kappa_F3x4_$(SEQ_LEN)sequences.pre
	for i in $^ ; do  printf ">"$$i"\n">>$@.tmp; bawk '$$2!="NA" {print $$seq_name, $$t, $$nucl_identity, $$codon_identity, $$AA_identity}' $$i | bsort -nk 3 >>$@.tmp ; done ;\
	printf "omega\trun\tt\tnucl_identity\tcodon_identity\tAA_identity\n" >>$@ ; \
	cat $@.tmp | sed 's/.*_seq-//g' | fasta2tab  | sed 's/omega.*pre//g' | bsort -nk 1  | uniq >>$@ ; rm $@.tmp

ALLomega_$(KAPPA)kappa_F3x4_30000_PAML_sequences.tab: ../../local/share/data/paml_out 
	printf "omega\trun\tt\tnucl_identity\tcodon_identity\tAA_identity\n" >$@
	../../local/src/analyse_paml_out.py $< | bsort -nk 3,3 | uniq >>$@

ALLomega_$(KAPPA)kappa_F3x4_$(SEQ_LEN)sequences.tab.pdf: ALLomega_$(KAPPA)kappa_F3x4_$(SEQ_LEN)sequences.tab
	cat $< | ../../local/src/vis_identity-compare.py -o $@

ALLomega_$(KAPPA)kappa_F3x4_30000_PAML_sequences.tab.pdf: ALLomega_$(KAPPA)kappa_F3x4_30000_PAML_sequences.tab
	cat $< | ../../local/src/vis_identity-compare.py -o $@
